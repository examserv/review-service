CREATE TABLE IF NOT EXISTS reviews(
    id serial primary key,
    post_id int,
    user_id int,
    name varchar(50),
    rating int CHECK (rating >= 1 and rating <= 5),
    description text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);