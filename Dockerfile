FROM golang:1.19-alpine
RUN mkdir review-servie
COPY . /review-servie
WORKDIR /review-servie
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 3333
