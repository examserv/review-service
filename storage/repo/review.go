package repo

import (
	pb "review-service/genproto/review"
)

type ReviewStorageI interface {
	CreateReview(*pb.ReviewReq) (*pb.ReviewRes, error)
	UpdateReview(*pb.ReviewRes) (*pb.ReviewRes, error)
	DeleteReviewPostId(*pb.ReviewPostId) (*pb.Empty, error)
	DeleteReviewUserId(*pb.ReviewUserId) (*pb.Empty, error)
	GetRewPostId(*pb.ReviewPostId) (*pb.Reviews, error)
    GetRewUserId(*pb.ReviewUserId) (*pb.Reviews, error)
}
