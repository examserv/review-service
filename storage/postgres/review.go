package postgres

import (
	"fmt"
	pb "review-service/genproto/review"

	"github.com/jmoiron/sqlx"
)

type reviewRepo struct {
	Db *sqlx.DB
}

func NewReviewRepo(db *sqlx.DB) *reviewRepo {
	return &reviewRepo{Db: db}
}

func (r *reviewRepo) CreateReview(review *pb.ReviewReq) (*pb.ReviewRes, error) {
	trx, err := r.Db.Begin()
	if err != nil {
		fmt.Println("error while inserting new customer by transaction")
		return &pb.ReviewRes{}, err
	}
	defer trx.Rollback()
	reviewResp := pb.ReviewRes{}
	err = trx.QueryRow(`
	insert into reviews (
		post_id,
		user_id,
		name,
		rating,
		description
		) 
	values ($1,$2,$3,$4,$5) 
		returning 
		id, post_id, user_id, name, rating, description`,
		review.PostId, review.UserId, review.Name, review.Rating, review.Description,
	).Scan(
		&reviewResp.Id,
		&reviewResp.PostId,
		&reviewResp.UserId,
		&reviewResp.Name,
		&reviewResp.Rating,
		&reviewResp.Description,
	)
	if err != nil {
		return &pb.ReviewRes{}, err
	}
	trx.Commit()
	return &reviewResp, nil
}

func (r *reviewRepo) GetRewPostId(req *pb.ReviewPostId) (*pb.Reviews, error) {
	reviews := &pb.Reviews{}
	rows, err := r.Db.Query(`
		select id, name, 
		description, 
		rating, 
		user_id, post_id 
		from reviews where post_id=$1`, req.PostId)
	if err != nil {
		return &pb.Reviews{}, err
	}
	for rows.Next() {
		review := &pb.ReviewRes{}
		rows.Scan(
			&review.Id,
			&review.Name,
			&review.Description,
			&review.Rating,
			&review.UserId, &review.PostId,
		)
		reviews.Reviews = append(reviews.Reviews, review)
	}
	defer rows.Close()
	return reviews, nil
}

func (r *reviewRepo) GetRewUserId(req *pb.ReviewUserId) (*pb.Reviews, error) {
	reviews := &pb.Reviews{}
	rows, err := r.Db.Query(`
		select id, name, 
		description, 
		rating, 
		user_id, post_id 
		from reviews where user_id=$1`, req.UserId)
	if err != nil {
		return &pb.Reviews{}, err
	}
	for rows.Next() {
		review := &pb.ReviewRes{}
		rows.Scan(
			&review.Id,
			&review.Name,
			&review.Description,
			&review.Rating,
			&review.UserId, &review.PostId,
		)
		reviews.Reviews = append(reviews.Reviews, review)
	}
	defer rows.Close()
	return reviews, nil
}

func (r *reviewRepo) UpdateReview(req *pb.ReviewRes) (*pb.ReviewRes, error) {
	// fmt.Println(req)
	user := pb.ReviewRes{}
	_, err := r.Db.Exec(`update reviews set
			updated_at=now(), 
			name = $1,
			post_id=$2,
			user_id=$3,
			description = $4,
			rating = $5
			where id = $6`, req.Name, req.PostId, req.UserId, req.Description, req.Rating, req.Id)
	if err != nil {
		return &pb.ReviewRes{}, err
	}
	return &user, nil
}

func (r *reviewRepo) DeleteReviewPostId(req *pb.ReviewPostId) (*pb.Empty, error) {
	_, err := r.Db.Exec(`UPDATE reviews SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, req.PostId)
	if err != nil {
		return &pb.Empty{}, err
	}
	return &pb.Empty{}, nil
}

func (r *reviewRepo) DeleteReviewUserId(req *pb.ReviewUserId) (*pb.Empty, error) {
	_, err := r.Db.Exec(`UPDATE reviews SET deleted_at=NOW() where user_id=$1 and deleted_at is null`, req.UserId)
	if err != nil {
		return &pb.Empty{}, err
	}
	return &pb.Empty{}, nil
}