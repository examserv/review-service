package service

import (
	"context"
	pb "review-service/genproto/review"
	l "review-service/pkg/logger"
	"review-service/storage"

	grpcclient "review-service/service/grpc_client"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ReviewService struct {
	storage storage.IStorage
	logger  l.Logger
	client  grpcclient.GrpcClientI
}

func NewReviewService(db *sqlx.DB, log l.Logger, client grpcclient.GrpcClientI) *ReviewService {
	return &ReviewService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
	}
}

func (s *ReviewService) CreateReview(ctx context.Context, req *pb.ReviewReq) (*pb.ReviewRes, error) {
	user, err := s.storage.Review().CreateReview(req)
	if err != nil {
		s.logger.Error("error insert review", l.Any("Error insert review", err))
		return &pb.ReviewRes{}, status.Error(codes.Internal, "something went wrong, please check review info")
	}
	return user, nil
}

func (s *ReviewService) GetRewPostId(ctx context.Context, req *pb.ReviewPostId) (*pb.Reviews, error) {
	review, err := s.storage.Review().GetRewPostId(req)
	if err != nil {
		s.logger.Error("error select", l.Any("Error select review", err))
		return &pb.Reviews{}, status.Error(codes.Internal, "something went wrong, please check review info")
	}
	return review, nil
}

func (s *ReviewService) GetRewUserId(ctx context.Context, req *pb.ReviewUserId) (*pb.Reviews, error) {
	review, err := s.storage.Review().GetRewUserId(req)
	if err != nil {
		s.logger.Error("error select", l.Any("Error select review", err))
		return &pb.Reviews{}, status.Error(codes.Internal, "something went wrong, please check review info")
	}
	return review, nil
}

func (s *ReviewService) UpdateReview(ctx context.Context, req *pb.ReviewRes) (*pb.ReviewRes, error) {
	// fmt.Println(req)
	review, err := s.storage.Review().UpdateReview(req)
	if err != nil {
		s.logger.Error("error update", l.Any("Error update review", err))
		return &pb.ReviewRes{}, status.Error(codes.Internal, "something went wrogn, please check review info")
	}
	return review, nil
}

func (s *ReviewService) DeleteReviewPostId(ctx context.Context, req *pb.ReviewPostId) (*pb.Empty, error) {
	review, err := s.storage.Review().DeleteReviewPostId(req)
	if err != nil {
		s.logger.Error("error delete", l.Any("Error delete review", err))
		return &pb.Empty{}, status.Error(codes.Internal, "something went wrogn, please check review info")
	}
	return review, nil
}
func (s *ReviewService) DeleteReviewUserId(ctx context.Context, req *pb.ReviewUserId) (*pb.Empty, error) {
	review, err := s.storage.Review().DeleteReviewUserId(req)
	if err != nil {
		s.logger.Error("error delete", l.Any("Error delete post review", err))
		return &pb.Empty{}, status.Error(codes.Internal, "something went wrogn, please check review info")
	}
	return review, nil
}
