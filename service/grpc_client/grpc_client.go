package grpcclient

import (
	"fmt"
	"review-service/config"
	pbc "review-service/genproto/customer"
	pbp "review-service/genproto/post"

	"google.golang.org/grpc"
)

type GrpcClientI interface {
	Post() pbp.PostServiceClient
	Customer() pbc.CustomerServiceClient
}

type GrpcClient struct {
	cfg             config.Config
	postService     pbp.PostServiceClient
	customerService pbc.CustomerServiceClient
}

func New(cfg config.Config) (*GrpcClient, error) {
	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("post service dial host:%s, port: %d", cfg.PostServiceHost, cfg.PostServicePort)
	}

	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.CustomerServiceHost, cfg.CustomerServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("post service dial host:%s, port: %d", cfg.CustomerServiceHost, cfg.CustomerServicePort)
	}

	return &GrpcClient{
		cfg:             cfg,
		postService:     pbp.NewPostServiceClient(connPost),
		customerService: pbc.NewCustomerServiceClient(connCustomer),
	}, nil
}

func (s *GrpcClient) Post() pbp.PostServiceClient {
	return s.postService
}
func (s *GrpcClient) Customer() pbc.CustomerServiceClient {
	return s.customerService
}
