package main

import (
	"net"
	"review-service/config"
	pb "review-service/genproto/review"
	"review-service/pkg/db"
	"review-service/pkg/logger"
	"review-service/service"
	grpcclient "review-service/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)
	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)
	connDB, err := db.ConnectToDB(cfg)

	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("grpc connection to client error", logger.Error(err))
	}
	reviewService := service.NewReviewService(connDB, log, grpcClient)

	listen, err := net.Listen("tcp", cfg.RPCHost+":"+cfg.RPCPort)

	if err != nil {
		log.Fatal("Error while listening:", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)

	pb.RegisterReviewServiceServer(s, reviewService)

	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))
	if err := s.Serve(listen); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
